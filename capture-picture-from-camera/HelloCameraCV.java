import org.opencv.core.Core;
import org.opencv.core.Mat;
import org.opencv.imgcodecs.Imgcodecs;
import org.opencv.videoio.VideoCapture;

import java.io.IOException;

public class HelloCameraCV {
    public void run() throws IOException {
        VideoCapture camera = new VideoCapture();
        camera.open(0);
        while (!camera.isOpened()){
            try {
                Thread.sleep(100);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        System.out.print("Press a key to capture a frame");
        System.in.read();
        Mat frame = new Mat();

        camera.grab();
        System.out.println("Frame Grabbed");
        camera.retrieve(frame);
        System.out.println("Frame Decoded");

        camera.read(frame);
        System.out.println("Frame Obtained");

    /* No difference
    camera.release();
    */

        System.out.println("Captured Frame Width " + frame.width());

        Imgcodecs.imwrite("camera.jpg", frame);
        System.out.println("OK");
    }

    public static void main(String[] args){

        System.out.println("Hello, OpenCV");
        // Load the native library.
        System.loadLibrary(Core.NATIVE_LIBRARY_NAME);
        try {
            new HelloCameraCV().run();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }
}
